import 'package:flutter/material.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class User {
  final String id;
  String name;
  bool isAdmin;
  bool sharingAudio;
  bool sharingVideo;
  bool forceMute;
  bool forceVideoOff;
  late final RTCPeerConnection peer;
  RTCDataChannel? dataChannel;
  late final RTCVideoRenderer? renderer;
  bool fullScreenMode;
  bool isSpeaking;

  User(this.id, this.isAdmin,
      {this.name = "",
      this.dataChannel,
      this.sharingAudio = true,
      this.sharingVideo = true,
      this.forceMute = false,
      this.forceVideoOff = false,
      this.fullScreenMode = false,
      this.isSpeaking = false});

  bool equalsUser(User user) {
    return id.compareTo(user.id) == 0;
  }

  @override
  String toString() {
    return '{id: $id, isAdmin: $isAdmin, userName: $name, sharingAudio: $sharingAudio, sharingVideo: $sharingVideo}';
  }
}
