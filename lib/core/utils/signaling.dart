import 'dart:developer';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:socket_io_client/socket_io_client.dart';

class Signaling {
  Socket? socket;

  static final instance = Signaling._();
  String userAgent = 'mobile';

  Signaling._();

  init(String url) {
    socket = io(
      url,
      OptionBuilder().setTransports(['websocket']).build(),
    );

    socket!.onConnect((data) {
      Fluttertoast.showToast(msg: 'connected');
      log('Connected!');
    });

    socket!.onError((data) {
      Fluttertoast.showToast(msg: 'error');
      log('Error: $data');
    });

    socket!.onConnectError((data) {
      Fluttertoast.showToast(msg: 'connection error');
      log('Connection error: $data');
    });

    socket!.onDisconnect((data) {
      Fluttertoast.showToast(msg: 'disconnected');
      log('Disconnected!');
    });

    socket!.connect();
  }

  joinRoom(int roomId, String userName) {
    print("the room id is :${roomId} ");
    socket!.emit('join room', {
      "roomID": roomId,
      "userName": userName,
      "voice_bool": true,
      "video_bool": true,
      "userAgent": userAgent
    });
  }

  sendOffer(RTCSessionDescription offer, String userToSignal) {
    log('sending offer...');
    final payload = {
      'userToSignal': userToSignal,
      'signal': {
        'sdp': offer.sdp,
        'type': offer.type,
      },
      'callerID': socket!.id,
    };

    socket!.emit('offer', payload);
    log('offer sent!');
  }

  sendAnswer(RTCSessionDescription answer, String userToSignal) {
    log('sending answer...');
    final payload = {
      'userToSignal': userToSignal,
      'signal': {
        'sdp': answer.sdp,
        'type': answer.type,
      },
      'callerID': socket!.id,
    };
    socket!.emit('answer', payload);
    log('answer sent!');
  }

  sendIceCandidate(RTCIceCandidate iceCandidate, String userToSignal) {
    log('sending ice...');
    final payload = {
      'userToSignal': userToSignal,
      'candidate': {
        'candidate': iceCandidate.candidate,
        'sdpMLineIndex': iceCandidate.sdpMLineIndex,
        'sdpMid': iceCandidate.sdpMid,
      },
    };
    socket!.emit('ice-candidate', payload);
    log('ice sent!');
  }

  toggleMic(bool voice_bool) {
    socket!.emit('toggle-voice', {'voice_bool': voice_bool});
  }

  toggleCam(bool video_bool) {
    socket!.emit('toggle-video', {'video_bool': video_bool});
  }

  endCall() {
    print("inside end call");
    socket!.emit('end-call', {});
    print("after inside end call");
  }

  kickOutUser(String userId) {
    socket!.emit('kick-out', {userId});
  }

  muteUser(String userToMute) {
    socket!.emit('mute-user', {userToMute});
  }

  unMuteUser(String userToUnmute) {
    socket!.emit('unmute-user', {userToUnmute});
  }

  muteAll() {
    socket!.emit('mute-all', {});
  }

  unmuteAll() {
    socket!.emit('unmute-all', {});
  }

  toggleCamUser(String userId, bool camOn) {
    if (camOn) {
      socket!.emit('cam-on-user', {userId});
    } else if (!camOn) {
      socket!.emit('cam-off-user', {userId});
    }
  }

  toggleCamAll(bool camOn) {
    if (camOn) {
      socket!.emit('cam-on-all', {});
    } else if (!camOn) {
      socket!.emit('cam-off-all', {});
    }
  }
}
