import 'package:flutter/material.dart';

class Custom{

  static buildButton ({required String title, required VoidCallback callBack}){
    return MaterialButton(
      minWidth: 150,
      color: Colors.amber,
      onPressed: callBack,
      child: Text(title),
    );
  }

  static buildAppBar({required String title}){
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.amber,
      title: Text(title),
    );
  }
}