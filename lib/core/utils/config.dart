final config = {
  // "iceTransportPolicy": "all", //all (default) - public - relay
  "rtcpMuxPolicy": "require", // negotiate (default) - require
  "bundlePolicy": "max-bundle", // max-bundle (default) - max-compat - relay
  // "sdpSemantics": "unified-plan", // unified-plan (default) - plan-b
  'iceServers': [
    {
      'urls': [
        'stun:stun1.l.google.com:19302',
        'stun:stun2.l.google.com:19302',
        'stun:stun3.l.google.com:19302',
        'stun:stun4.l.google.com:19302',
      ],
    },
    {
      'urls': "turn:standard.relay.metered.ca:80",
      'username': "86721531a8351f5134873628",
      'credential': "glTfzJOpzAiRVSBE",
    },
    {
      'urls': "turn:standard.relay.metered.ca:80?transport=tcp",
      'username': "86721531a8351f5134873628",
      'credential': "glTfzJOpzAiRVSBE",
    },
    {
      'urls': "turn:standard.relay.metered.ca:443",
      'username': "86721531a8351f5134873628",
      'credential': "glTfzJOpzAiRVSBE",
    },
    {
      'urls': "turns:standard.relay.metered.ca:443?transport=tcp",
      'username': "86721531a8351f5134873628",
      'credential': "glTfzJOpzAiRVSBE",
    },
  ],
};
