import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/DTOs/user.dart';
import 'package:flutter_mesh2/core/pages/widgets/full_screen_video.dart';
import 'package:flutter_mesh2/core/utils/signaling.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class RemoteVideoCard extends StatefulWidget {
  User user;
  bool localUserAmin;
  final VoidCallback toggleFullScreen;
  RemoteVideoCard(this.user, this.toggleFullScreen, this.localUserAmin);

  @override
  State<StatefulWidget> createState() => _RemoteViewCardState();
}

class _RemoteViewCardState extends State<RemoteVideoCard> {
  bool fullScreenIcon = false;
  @override
  Widget build(BuildContext context) {
    if (widget.user.fullScreenMode) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FullScreenVideoPage(user: widget.user),
          ),
        ).then((_) {
          setState(() {
            widget.user.fullScreenMode = false;
            fullScreenIcon = false;
          });
        });
      });
      return Container(); // Return an empty container while navigating
    } else {
      return buildNormalView();
    }
  }

  Widget buildNormalView() {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(10),
          // height: 250,
          // width: 150,
          child: widget.user.renderer != null
              ? FittedBox(
                  fit: BoxFit.cover,
                  child: Container(
                    height: 170,
                    width: 200,
                    child: Stack(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              fullScreenIcon = !fullScreenIcon;
                            });
                          },
                          child: Transform(
                            transform: Matrix4.identity()..rotateY(0.0),
                            // alignment: FractionalOffset.center,
                            child: RTCVideoView(
                              widget.user.renderer!,
                              objectFit: RTCVideoViewObjectFit
                                  .RTCVideoViewObjectFitCover,
                              placeholderBuilder: (context) {
                                return CircularProgressIndicator(
                                  color: Colors.purpleAccent,
                                );
                              },
                            ),
                          ),
                        ),
                        fullScreenIcon
                            ? Center(
                                child: IconButton(
                                    color: Colors.black,
                                    iconSize: 30,
                                    onPressed: () {
                                      setState(() {
                                        widget.user.fullScreenMode = true;
                                      });
                                    },
                                    icon: Icon(Icons.fullscreen_outlined)),
                              )
                            : Container(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Icon(
                                Icons.mic,
                                color: widget.user.sharingAudio
                                    ? Colors.grey
                                    : Colors.red,
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 8.0),
                              child: Icon(
                                Icons.camera_alt_outlined,
                                color: widget.user.sharingVideo
                                    ? Colors.grey
                                    : Colors.red,
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            widget.localUserAmin
                                ? PopupMenuButton<String>(
                                    onSelected: (String value) {
                                      switch (value) {
                                        case 'kick-out':
                                          Signaling.instance
                                              .kickOutUser(widget.user.id);
                                          break;
                                        case 'mute-user':
                                          Signaling.instance
                                              .muteUser(widget.user.id);
                                          break;
                                        case 'unmute-user':
                                          Signaling.instance
                                              .unMuteUser(widget.user.id);
                                          break;
                                        case 'cam-on-user':
                                          Signaling.instance.toggleCamUser(
                                              widget.user.id, true);
                                          break;
                                        case 'cam-off-user':
                                          Signaling.instance.toggleCamUser(
                                              widget.user.id, false);

                                          break;
                                      }
                                    },
                                    itemBuilder: (BuildContext context) =>
                                        <PopupMenuEntry<String>>[
                                      PopupMenuItem<String>(
                                        value: 'kick-out',
                                        child: Text('kick-out'),
                                      ),
                                      PopupMenuItem<String>(
                                        value: 'mute-user',
                                        child: Text('mute'),
                                      ),
                                      PopupMenuItem<String>(
                                        value: 'unmute-user',
                                        child: Text('unmute'),
                                      ),
                                      PopupMenuItem<String>(
                                        value: 'cam-on-user',
                                        child: Text('cam-on'),
                                      ),
                                      PopupMenuItem<String>(
                                        value: 'cam-off-user',
                                        child: Text('cam-off'),
                                      ),
                                    ],
                                    icon: Icon(
                                      Icons.settings_outlined,
                                      size: 15,
                                    ),
                                  )
                                : SizedBox(
                                    width: 5,
                                  ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              children: [
                                Container(
                                  width: 80,
                                  height: 40,
                                  padding: EdgeInsets.symmetric(horizontal: 5),
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(20)),
                                      color: Colors.grey[800]),
                                  child: Center(
                                    child: Text(
                                      widget.user.name,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                widget.user.isSpeaking
                                    ? Positioned(
                                        top: 10,
                                        right: 10,
                                        child: Image.asset(
                                          'assets/gif/voice_vibration.gif',
                                          height: 24.0,
                                          width: 24.0,
                                        ),
                                      )
                                    : SizedBox.shrink(),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                )
              : const Placeholder(),
        ),

        // widget.localUserAmin ? Container()
      ],
    );
  }

  // Widget buildFullScreenView(BuildContext context) {
  //   final Size screenSize = MediaQuery.of(context).size;

  //   return Scaffold(
  //     body: Stack(
  //       children: [
  //         InteractiveViewer(
  //           boundaryMargin: EdgeInsets.all(20.0),
  //           minScale: 0.1,
  //           maxScale: 4.0,
  //           child: RTCVideoView(
  //             widget.user.renderer!,
  //             objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
  //             placeholderBuilder: (context) {
  //               return CircularProgressIndicator(
  //                 color: Colors.purpleAccent,
  //               );
  //             },
  //           ),
  //         ),
  //         Positioned(
  //           top: 10,
  //           right: 10,
  //           child: IconButton(
  //             icon: Icon(Icons.fullscreen_exit),
  //             onPressed: () {
  //               setState(() {
  //                 widget.user.fullScreenMode = false;
  //                 fullScreenIcon = false;
  //               });
  //             },
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}
