import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/DTOs/user.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class FullScreenVideoPage extends StatefulWidget {
  final User user;

  FullScreenVideoPage({required this.user});

  @override
  State<FullScreenVideoPage> createState() => _FullScreenVideoPageState();
}

class _FullScreenVideoPageState extends State<FullScreenVideoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: RotatedBox(
              quarterTurns: 4,
              child: RTCVideoView(
                widget.user.renderer!,
                objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitContain,
                placeholderBuilder: (context) {
                  return CircularProgressIndicator(
                    color: Colors.purpleAccent,
                  );
                },
              ),
            ),
          ),
          Positioned(
            top: 10,
            right: 10,
            child: IconButton(
              icon: Icon(Icons.fullscreen_exit),
              onPressed: () {
                setState(() {
                  widget.user.fullScreenMode = false;
                  Navigator.pop(context);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
