import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/pages/call.dart';
import 'package:flutter_mesh2/core/utils/custom.dart';
import 'package:flutter_mesh2/core/utils/signaling.dart';

class JoinRoom extends StatelessWidget {
  final bool newRoom;
  final roomIdController = TextEditingController();
  final nameController = TextEditingController();

  JoinRoom({super.key, required this.newRoom});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Custom.buildAppBar(title: newRoom ? 'Create Room' : 'Join Room'),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 300,
              child: TextField(
                controller: roomIdController,
                decoration: const InputDecoration(
                  hintText: 'Room ID',
                ),
              ),
            ),
            SizedBox(
              width: 300,
              child: TextField(
                controller: nameController,
                decoration: const InputDecoration(
                  hintText: 'Your Name',
                ),
              ),
            ),
            const SizedBox(height: 20),
            Custom.buildButton(
              title: newRoom ? 'Create' : 'Join',
              callBack: () {
                int roomId = int.parse(roomIdController.text.toString());
                String userName = nameController.text;
                Signaling.instance.joinRoom(roomId, userName);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Call(
                          roomId: roomIdController.text,
                          name: nameController.text)),
                );
              },
            ),
            const SizedBox(height: 5),
            Custom.buildButton(
                title: 'Back',
                callBack: () {
                  Navigator.of(context).pop();
                }),
          ],
        ),
      ),
    );
  }
}
