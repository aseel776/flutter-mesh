import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/pages/join_room.dart';
import 'package:flutter_mesh2/core/utils/custom.dart';
import 'package:permission_handler/permission_handler.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Custom.buildAppBar(title: 'Mesh Video Call'),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Custom.buildButton(
              title: 'Create room',
              callBack: () {
                moveToJoinRoom(context, newRoom: true);
              },
            ),
            const SizedBox(height: 5),
            Custom.buildButton(
              title: 'Join room',
              callBack: () {
                moveToJoinRoom(context, newRoom: false);
              },
            ),
          ],
        ),
      ),
    );
  }

  moveToJoinRoom(BuildContext context, {required bool newRoom}) async {
    Map<Permission, PermissionStatus> statuses = await [
      // Permission.camera,
      Permission.microphone,
      // Add more permissions as needed
    ].request();

    // Check if all permissions are granted
    bool allPermissionsGranted =
        statuses.values.every((status) => status.isGranted);
    if (allPermissionsGranted) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => JoinRoom(newRoom: newRoom)),
      );
    } else {
      statuses.forEach((permission, status) {
        if (status.isDenied) {
          print('$permission permission is denied.');
        } else if (status.isPermanentlyDenied) {
          print('$permission permission is permanently denied.');
          // Handle if user denied the permission permanently
        }
      });
    }
  }
}
