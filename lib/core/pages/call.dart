import 'dart:async';
import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/DTOs/user.dart';
import 'package:flutter_mesh2/core/pages/widgets/remote_video_card.dart';
import 'package:flutter_mesh2/core/utils/config.dart';
import 'package:flutter_mesh2/core/utils/signaling.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'dart:convert';

class Call extends StatefulWidget {
  final String roomId;
  final String name;

  const Call({super.key, required this.roomId, required this.name});

  @override
  State<Call> createState() => _CallState();
}

class _CallState extends State<Call> with TickerProviderStateMixin {
  //media constraints
  bool hasAudioInput = true,
      isAudioOn = true,
      hasVideoInput = true,
      isVideoOn = true,
      isFrontCameraSelected = true;
  bool forceVideoOff = false, forceMute = false;
  RTCVideoRenderer _localVideoRenderer = RTCVideoRenderer();
  // RTCPeerConnection? _localPeerConnection;
  // RTCDataChannel? _localDataChannel;
  MediaStream? _localStream;
  List<MediaDeviceInfo> _audioDevices = [];
  List<MediaDeviceInfo> _videoDevices = [];
  bool localUserIsAdmin = false;

  List<User> users = [];
  List<User> connectedUsers = [];
  List<User> failedUsers = [];
  bool isLoading = true;
  // List<Map<String, dynamic>> dataChannels = [];
  late AnimationController bottomSheetController;

  @override
  void initState() {
    setUpEvents();
    WidgetsFlutterBinding.ensureInitialized().addPostFrameCallback((_) async {
      await setupMedia();
    });
    bottomSheetController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 800));
    ;

    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  void dispose() {
    _localVideoRenderer.dispose();
    _localStream!.dispose();
    for (var user in users) {
      user.peer.dispose();
      user.peer.close();
      user.renderer!.dispose();
    }
    users.clear();
    users = [];
    bottomSheetController.dispose();
    super.dispose();
  }

  _getMediaDevices() async {
    List<MediaDeviceInfo> devices =
        await navigator.mediaDevices.enumerateDevices();

    _audioDevices =
        devices.where((device) => device.kind == 'audioinput').toList();
    _videoDevices =
        devices.where((device) => device.kind == 'videoinput').toList();

    setState(() {});
  }

  setupMedia() async {
    _localVideoRenderer.initialize();
    await setMediaConstraints();
    try {
      _localStream = await navigator.mediaDevices.getUserMedia({
        'audio': hasAudioInput && isAudioOn,
        'video': hasVideoInput && isVideoOn
            ? {'facingMode': isFrontCameraSelected ? 'user' : 'environment'}
            : false,
      });
      _localVideoRenderer.srcObject = _localStream;
      setState(() {});
    } catch (e) {
      log(e.toString());
      Fluttertoast.showToast(msg: e.toString());
    }
  }

  setMediaConstraints() async {
    final enumeratedDevices = await navigator.mediaDevices.enumerateDevices();
    hasAudioInput = enumeratedDevices
        .where((device) => device.kind == 'audioinput')
        .isNotEmpty;
    hasVideoInput = enumeratedDevices
        .where((device) => device.kind == 'videoinput')
        .isNotEmpty;
  }

  setUpEvents() {
    if (!Signaling.instance.socket!.connected) {
      Signaling.instance.socket!.connect();
    }

    Signaling.instance.socket!.on('all users', (data) async {
      Fluttertoast.showToast(msg: 'all users!!!');
      log('all users!!!');
      handleAllUsers(data);
      await setupPeers();
      setState(() {});
    });

    Signaling.instance.socket!.on('offer', (data) async {
      Fluttertoast.showToast(msg: 'offer!!!');
      log('offer!!!');
      await handleOffer(data);
      setState(() {});
    });

    Signaling.instance.socket!.on('answer', (data) async {
      Fluttertoast.showToast(msg: 'answer!!!');
      log('answer!!!');
      await handleAnswer(data);
      setState(() {});
    });

    Signaling.instance.socket!.on('ice-candidate', (data) async {
      Fluttertoast.showToast(msg: 'ICE!!!');
      log('ICE!!!');
      var user = users.singleWhere((user) => user.id.compareTo(data['id']) == 0,
          orElse: () => User('-1', false));
      if (user.id.compareTo('-1') != 0) {
        await handleIceCandidate(data);
      }
      setState(() {});
    });

    Signaling.instance.socket!.on('user-leave', (data) {
      Fluttertoast.showToast(msg: 'user leave!!!');
      log('user leave!!!');
      handleUserLeave(data);
      setState(() {});
    });
    Signaling.instance.socket!.on('force-leave', (data) => forceLeave());
    Signaling.instance.socket!.on('force-mute', (data) => forceMuteFunc());
    Signaling.instance.socket!.on('unmute', (data) => forceUnmute());
    Signaling.instance.socket!.on('force-cam-off', (data) => forceCamOff());
    Signaling.instance.socket!.on('cam-on', (data) => forceCamOn());
  }

  setupPeers() async {
    log('setting peers');
    for (var user in users) {
      log('user ${user.id}');
      await setupConnection(user);
      await setupOffer(user);
    }
    log('peers set');
  }

  createDataChannel(User user) async {
    // int index = dataChannels.indexWhere((item) => item['Id'] == user.id);
    if (user.dataChannel == null) {
      // dataChannels.add({"id": user.id, "dataChannel": null});
      RTCDataChannelInit dataChannelInit = RTCDataChannelInit();
      user.dataChannel =
          await user.peer.createDataChannel("data_channel", dataChannelInit);
      user.dataChannel!.onMessage = (RTCDataChannelMessage message) {
        Map<String, dynamic> decodedMessage = jsonDecode(message.text);
        if (decodedMessage['type'] == 'voice-toggle') {
          print("decoded message:${decodedMessage['data']['voice_bool']}");
          print("decoded message:${decodedMessage['data']['id']}");
          // print("decoded message::")
          // user.sharingAudio = voiceBool;
          // setState(() {
          bool voiceBool = decodedMessage['data']['voice_bool'];
          // int index = users.indexWhere(
          //     (element) => element.id == decodedMessage['data']['id']);
          // users[index].sharingAudio = voiceBool;
          user.sharingAudio = voiceBool;
          print('Received voice toggle message with value: $voiceBool');
          // });
        } else if (decodedMessage['type'] == 'video-toggle') {
          print("decoded message:${decodedMessage['data']['video_bool']}");

          setState(() {
            bool videoBool = decodedMessage['data']['video_bool'];
            // int index = users.indexWhere(
            //     (element) => element.id == decodedMessage['data']['id']);
            // users[index].sharingVideo = videoBool;
            user.sharingVideo = videoBool;
            print('Received video toggle message with value: $videoBool');
          });
        }
        setState(() {});
        log("Received message: ${message.toString()}");
        print("Received message: ${message.text}");
      };
    }
    setState(() {});
  }

  sendMessageToAllDataChannels(bool camOrAudio, bool onOff) {
    users.forEach((user) async {
      RTCDataChannel? dataChannel = user.dataChannel;
      if (dataChannel != null &&
          dataChannel.state == RTCDataChannelState.RTCDataChannelOpen) {
        if (Signaling.instance.socket!.connected) {
          String userId = Signaling.instance.socket!.id!;
          String messageString;
          if (camOrAudio) {
            messageString = jsonEncode({
              "type": "video-toggle",
              "data": {
                "id": userId,
                "video_bool": onOff,
              },
            });
          } else {
            messageString = jsonEncode({
              "type": "voice-toggle",
              "data": {
                "id": userId,
                "voice_bool": onOff,
              },
            });
          }

          await dataChannel.send(RTCDataChannelMessage(messageString));
        }
      }
    });
    setState(() {});
  }

  setupConnection(User user) async {
    log('setting connection for user ${user.id}');

    user.renderer = RTCVideoRenderer();
    user.renderer!.initialize();
    setState(() {});
    user.peer = await createPeerConnection(config);
    user.peer.onConnectionState = (state) {
      if (state == RTCPeerConnectionState.RTCPeerConnectionStateConnected) {
        log("${user.name} is :" + state.toString());
        print("${user.name} is :" + state.toString());
        connectedUsers.add(user);
        setState(() {});
      } else if (state == RTCIceConnectionState.RTCIceConnectionStateFailed ||
          state == RTCIceConnectionState.RTCIceConnectionStateDisconnected) {
        log("${user.name} is :" + state.toString());
        print("${user.name} is :" + state.toString());
        failedUsers.add(user);
        setState(() {});
      }
    };
    user.peer.onTrack = (event) async {
      user.renderer!.srcObject = event.streams[0];

      // List<RTCRtpReceiver> list = await user.peer.getReceivers();
      // for (var value in list) {
      //   print("all the value ${value.toString()}");
      //   // if (value.track!.kind == 'audio') {
      //   List<StatsReport> list2 = await value.getStats();

      //   for (var value2 in list2) {
      //     print("value2 only :${value2.values.toString()}");
      //     debugPrint(value2.values.toString());
      //     print("audio kind: ${value2.values["kind"]}");
      //     if (value2.values["kind"] == 'audio') {
      //       print("audio level : ${value2.values["audioLevel"]}");
      //       if (value2.values["audioLevel"] > 0) {
      //         setState(() {
      //           user.isSpeaking = true;
      //         });
      //       } else {
      //         user.isSpeaking = false;
      //       }
      //     }
      //   }
      //   // }
      // }
      // ;
      setState(() {});
    };

    user.peer.onDataChannel = (dataChannel) {
      print("onDataChannel");
      log("onDataChannel");

      user.dataChannel = dataChannel;
      if (user.dataChannel != null) {
        user.dataChannel!.onDataChannelState = (state) {
          log(state.toString());
          print(state.toString());
        };
        user.dataChannel!.onMessage = (RTCDataChannelMessage message) {
          Map<String, dynamic> decodedMessage = jsonDecode(message.text);
          if (decodedMessage['type'] == 'voice-toggle') {
            print("decoded message:${decodedMessage['data']['voice_bool']}");
            bool voiceBool = decodedMessage['data']['voice_bool'];
            user.sharingAudio = voiceBool;
            print('Received voice toggle message with value: $voiceBool');
          } else if (decodedMessage['type'] == 'video-toggle') {
            print("decoded message:${decodedMessage['data']['video_bool']}");
            bool videoBool = decodedMessage['data']['video_bool'];
            user.sharingVideo = videoBool;
            print('Received video toggle message with value: $videoBool');
          }
          log("on data channel");
          log("Received message: ${message.toString()}");
          print("Received message: ${message.text}");
          setState(() {});
        };
      }
      ;
      setState(() {});
    };

    while (_localStream == null) {
      await Future.delayed(const Duration(milliseconds: 500));
    }
    _localStream!.getTracks().forEach((track) async {
      await user.peer.addTrack(track, _localStream!);
    });

    user.peer.onIceCandidate = (candidate) async {
      await Signaling.instance.sendIceCandidate(candidate, user.id);
    };
    Timer.periodic(const Duration(seconds: 1), (timer) async {
      List<StatsReport>? stats = await user.peer.getStats();
      // print("error");
      // print("list length${stats.length}");
      if (!user.sharingAudio) {
        setState(() {
          user.isSpeaking = false;
        });
        return;
      }
      stats.forEach((element) {
        // print("element ${element.toString()}");
        //element.type == "ssrc" &&element.values["mediaType"] == "audio"
        if (element.values["audioLevel"] != null) {
          print(
              'audio level: ${element.values["audioLevel"]} for user ${user.name}');
          print("============================");
          if (element.values["audioLevel"] > 0.01) {
            setState(() {
              user.isSpeaking = true;
            });
          } else {
            setState(() {
              user.isSpeaking = false;
            });
          }
        }
      });
    });

    log('connection set for user ${user.id}');
  }

  setupOffer(User user) async {
    await createDataChannel(user);
    log('setting offer for user ${user.id}');

    final options = {
      'offerToReceiveAudio': true,
      'offerToReceiveVideo': true,
    };
    RTCSessionDescription offer = await user.peer.createOffer(options);
    await user.peer.setLocalDescription(offer);
    Signaling.instance.sendOffer(offer, user.id);

    log('offer set for user ${user.id}');
  }

  handleAllUsers(List<dynamic> rawData) {
    log('handling users');
    if (rawData.isEmpty) {
      localUserIsAdmin = true;
    }
    if (rawData.isNotEmpty) {
      print("user map : ${rawData[0]}");
    }
    users.addAll(rawData.map((user) => User(user['id'], user['isAdmin'],
        name: user['userName'],
        sharingAudio: user['voice'],
        sharingVideo: user['video'])));
    log('users: ${users.map((user) => user.toString() + '\n')}');
    log('users handled');
  }

  handleOffer(dynamic rawData) async {
    log('handling offer');
    Map<String, dynamic> temp = rawData['signal'];
    RTCSessionDescription offer =
        RTCSessionDescription(temp['sdp'], temp['type']);
    final userId = rawData['callerID'];
    final isAdmin = rawData['isAdmin'];
    var userName = rawData['userName'];
    var sharingAudio = rawData['voice'];
    var sharingVideo = rawData['video'];

    final newUser = User(userId, isAdmin,
        name: userName, sharingAudio: sharingAudio, sharingVideo: sharingVideo);
    await setupConnection(newUser);
    await newUser.peer.addTransceiver(
      kind: RTCRtpMediaType.RTCRtpMediaTypeAudio,
      init: RTCRtpTransceiverInit(
        direction: TransceiverDirection.RecvOnly,
      ),
    );
    await newUser.peer.addTransceiver(
      kind: RTCRtpMediaType.RTCRtpMediaTypeVideo,
      init: RTCRtpTransceiverInit(
        direction: TransceiverDirection.RecvOnly,
      ),
    );
    await newUser.peer.setRemoteDescription(offer);
    await setupAnswer(newUser, userId);
    users.add(newUser);
    log('offer handled');
  }

  setupAnswer(User user, String userId) async {
    log('setting answer');
    RTCSessionDescription answer = await user.peer.createAnswer();
    await user.peer.setLocalDescription(answer);
    Signaling.instance.sendAnswer(answer, userId);
    log('answer set');
  }

  handleAnswer(dynamic rawData) async {
    log('handling answer');
    Map<String, dynamic> temp = rawData['signal'];
    RTCSessionDescription answer =
        RTCSessionDescription(temp['sdp'], temp['type']);
    final userId = rawData['id'];

    await users
        .singleWhere((user) => user.id.compareTo(userId) == 0)
        .peer
        .setRemoteDescription(answer);
    log('answer handled');
  }

  handleIceCandidate(dynamic rawData) async {
    log('handling ice');
    Map<String, dynamic> temp = rawData['candidate'];
    RTCIceCandidate candidate = RTCIceCandidate(
        temp['candidate'], temp['sdpMid'], temp['sdpMLineIndex']);
    final userId = rawData['id'];

    await users
        .singleWhere((user) => user.id.compareTo(userId) == 0)
        .peer
        .addCandidate(candidate);
    log('ice handled');
  }

  handleUserLeave(dynamic rawData) {
    log('handling user $rawData leave');
    String userId = rawData;
    int index = users.indexWhere((user) => user.id.compareTo(userId) == 0);
    users.removeAt(index);
    log('user $rawData leave handled');
  }

  forceLeave() {
    print("force leave for id : ${Signaling.instance.socket!.id}");
    setState(() {
      forceMute = true;
      forceVideoOff = true;
    });
    _localStream!.getTracks().forEach((track) async {
      await track.stop();
    });
    Signaling.instance.socket!.disconnect();
    Navigator.popUntil(
      context,
      (route) => route.isFirst,
    );
  }

  forceMuteFunc() async {
    forceMute = true;
    if (isAudioOn) {
      setState(() {
        isAudioOn = false;
      });
    }
    await sendMessageToAllDataChannels(false, isAudioOn);
    _localStream!.getAudioTracks().forEach((track) async {
      track.enabled = false;
    });
    setState(() {});
  }

  forceUnmute() async {
    forceMute = false;
    if (!isAudioOn) {
      setState(() {
        isAudioOn = false;
      });
    }
    await sendMessageToAllDataChannels(false, isAudioOn);
    _localStream!.getAudioTracks().forEach((track) async {
      track.enabled = false;
    });
    setState(() {});
  }

  forceCamOff() async {
    forceVideoOff = true;
    if (isVideoOn) {
      setState(() {
        isVideoOn = false;
      });
    }
    await sendMessageToAllDataChannels(true, isVideoOn);
    _localStream!.getVideoTracks().forEach((track) {
      track.enabled = false;
    });
    setState(() {});
  }

  forceCamOn() async {
    forceVideoOff = false;
    if (!isVideoOn) {
      setState(() {
        isVideoOn = false;
      });
    }
    await sendMessageToAllDataChannels(true, isVideoOn);

    _localStream!.getVideoTracks().forEach((track) {
      track.enabled = false;
    });
    setState(() {});
  }

  _toggleMic() async {
    if (forceMute) {
      return;
    }
    isAudioOn = !isAudioOn;
    await sendMessageToAllDataChannels(false, isAudioOn);
    Signaling.instance.toggleMic(isAudioOn);
    _localStream?.getAudioTracks().forEach((track) {
      track.enabled = isAudioOn;
    });

    setState(() {});
  }

  _toggleCamera() async {
    if (forceVideoOff) {
      return;
    }
    isVideoOn = !isVideoOn;
    await sendMessageToAllDataChannels(true, isVideoOn);
    Signaling.instance.toggleCam(isVideoOn);
    _localStream?.getVideoTracks().forEach((track) {
      track.enabled = isVideoOn;
    });
    setState(() {});
  }

  _switchCamera() {
    isFrontCameraSelected = !isFrontCameraSelected;
    _localStream?.getVideoTracks().forEach((track) {
      Helper.switchCamera(
        _localVideoRenderer.srcObject!.getVideoTracks()[0],
        null,
        _localStream,
      );
    });
    setState(() {});
  }

  _endCall() async {
    print("before end call");
    await Signaling.instance.endCall();
    print("after end call");
    _localStream!.dispose();
    _localStream!.dispose();
    Signaling.instance.socket!.off('all users');
    Signaling.instance.socket!.off('offer');
    Signaling.instance.socket!.off('answer');
    Signaling.instance.socket!.off('ice-candidate');
    Signaling.instance.socket!.off('user-leave');
    Signaling.instance.socket!.disconnect();
    Navigator.pop(context);
  }

  bool checkConnectedUser() {
    if (connectedUsers.length == users.length) {
      setState(() {
        isLoading = false;
      });
      return true;
    } else if (failedUsers.length != 0) {
      setState(() {
        isLoading = false;
      });
      return false;
    } else {
      return false;
    }
  }

  bool _isListVisible = true;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  PersistentBottomSheetController? _bottomSheetController;

  @override
  Widget build(BuildContext context) {
    // bool validConnection = checkConnectedUser();
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Row(
          children: [
            Signaling.instance.socket != null &&
                    Signaling.instance.socket!.id != null
                ? Text(
                    "MyId: ${Signaling.instance.socket!.id!.substring(0, 5)}")
                : Container(),
            SizedBox(
              width: 20,
            ),
            Text("Name: ${widget.name}")
          ],
        ),
        // leading: Text("MyId:"),
        actions: [
          localUserIsAdmin
              ? PopupMenuButton<String>(
                  onSelected: (String value) {
                    switch (value) {
                      case 'mute-all':
                        Signaling.instance.muteAll();
                        break;
                      case 'unmute-all':
                        Signaling.instance.unmuteAll();
                        break;
                      case 'cam-on-all':
                        Signaling.instance.toggleCamAll(true);
                        break;
                      case 'cam-off-all':
                        Signaling.instance.toggleCamAll(false);
                        break;
                    }
                  },
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry<String>>[
                    PopupMenuItem<String>(
                      value: 'mute-all',
                      child: Text('mute-all'),
                    ),
                    PopupMenuItem<String>(
                      value: 'unmute-all',
                      child: Text('unmute-all'),
                    ),
                    PopupMenuItem<String>(
                      value: 'cam-on-all',
                      child: Text('cam-on-all'),
                    ),
                    PopupMenuItem<String>(
                      value: 'cam-off-all',
                      child: Text('cam-off-all'),
                    ),
                  ],
                  icon: Icon(Icons.settings_outlined),
                )
              : Container(),
        ],
      ),
      body: SafeArea(
        child: Stack(
          children: [
            // Positioned(
            //   top: 0,
            //   child: Container(
            //     height: 50,
            //     width: width,
            //     color: Colors.white,
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.spaceAround,
            //       children: [
            //         Text("MyId: ${Signaling.instance.socket!.id}")
            //         // if
            //       ],
            //     ),
            //   ),
            // ),
            RTCVideoView(
              _localVideoRenderer,
              objectFit: RTCVideoViewObjectFit.RTCVideoViewObjectFitCover,
              mirror: true,
              placeholderBuilder: (context) {
                return SizedBox(
                    height: 80, width: 80, child: CircularProgressIndicator());
              },
            ),
            Positioned(
              bottom: 60,
              child: ElevatedButton(
                onPressed: () {
                  bottomSheetController.forward();
                  // _buildRemoteUsersBottomSheet();
                  // _showRemoteUsersBottomSheet(context);
                },
                child: Text('Show Remote Users'),
              ),
            ),
            // _buildRemoteUsersBottomSheet(),
            LayoutBuilder(builder: (context, constraints) {
              return Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: users.length,
                  itemBuilder: (context, index) {
                    User user = users[index];
                    return Container(
                      // padding: const EdgeInsets.all(10),
                      // height: width * .1,
                      // width: width * .3,
                      child: user.renderer != null
                          ? RemoteVideoCard(user, () {
                              setState(() {
                                user.fullScreenMode = !user.fullScreenMode;
                              });
                            }, localUserIsAdmin)
                          : const SizedBox(),
                    );
                    // return Positioned(
                    //   left: _offset.dx,
                    //   top: _offset.dy,
                    //   child: LongPressDraggable<int>(
                    //     data: index,
                    //     child:
                    //     feedback: Container(
                    //       padding: const EdgeInsets.all(10),
                    //       height: 150,
                    //       width: 100,
                    //       child: user.renderer != null
                    //           ? RTCVideoView(
                    //               user.renderer!,
                    //               objectFit: RTCVideoViewObjectFit
                    //                   .RTCVideoViewObjectFitCover,
                    //             )
                    //           : const Placeholder(),
                    //     ),
                    //     onDragEnd: (details) {
                    //       setState(() {
                    //         double adjustment = MediaQuery.of(context).size.height -
                    //             constraints.maxHeight;
                    //         _offset = Offset(
                    //             details.offset.dx, details.offset.dy - adjustment);
                    //       });
                    //     },
                    //     // childWhenDragging: Container(),
                    //   ),
                    // );
                  },
                ),
              );
            }),

            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.start,
            //   children: [
            //     Row(
            //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //       children: [
            //         Text(
            //           'Remote Users',
            //           style:
            //               TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            //         ),
            //         IconButton(
            //           icon: Transform.rotate(
            //             angle: _isListVisible
            //                 ? 0
            //                 : 3.14, // Rotate arrow icon based on visibility state
            //             child: Icon(Icons.arrow_drop_down),
            //           ),
            //           onPressed: () {
            //             setState(() {
            //               _isListVisible =
            //                   !_isListVisible; // Toggle list visibility
            //             });
            //           },
            //         ),
            //       ],
            //     ),
            //     AnimatedContainer(
            //       duration: Duration(milliseconds: 300), // Animation duration
            //       height: _isListVisible
            //           ? 150
            //           : 0, // Set height based on visibility state
            //       child: LayoutBuilder(builder: (context, constraints) {
            //         return Padding(
            //           padding: const EdgeInsets.only(left: 8.0),
            //           child: ListView.builder(
            //             scrollDirection: Axis.horizontal,
            //             itemCount: users.length,
            //             itemBuilder: (context, index) {
            //               User user = users[index];
            //               return Container(
            //                 // padding: const EdgeInsets.all(10),
            //                 // height: width * .1,
            //                 // width: width * .3,
            //                 child: user.renderer != null
            //                     ? RemoteVideoCard(user, () {
            //                         setState(() {
            //                           user.fullScreenMode =
            //                               !user.fullScreenMode;
            //                         });
            //                       }, localUserIsAdmin)
            //                     : const SizedBox(),
            //               );
            //               // return Positioned(
            //               //   left: _offset.dx,
            //               //   top: _offset.dy,
            //               //   child: LongPressDraggable<int>(
            //               //     data: index,
            //               //     child:
            //               //     feedback: Container(
            //               //       padding: const EdgeInsets.all(10),
            //               //       height: 150,
            //               //       width: 100,
            //               //       child: user.renderer != null
            //               //           ? RTCVideoView(
            //               //               user.renderer!,
            //               //               objectFit: RTCVideoViewObjectFit
            //               //                   .RTCVideoViewObjectFitCover,
            //               //             )
            //               //           : const Placeholder(),
            //               //     ),
            //               //     onDragEnd: (details) {
            //               //       setState(() {
            //               //         double adjustment = MediaQuery.of(context).size.height -
            //               //             constraints.maxHeight;
            //               //         _offset = Offset(
            //               //             details.offset.dx, details.offset.dy - adjustment);
            //               //       });
            //               //     },
            //               //     // childWhenDragging: Container(),
            //               //   ),
            //               // );
            //             },
            //           ),
            //         );
            //       }),
            //     ),
            //   ],
            // ),

            // if (isLoading)
            //   Container(
            //     color: Colors.black.withOpacity(0.5),
            //     child: Center(
            //       child: CircularProgressIndicator(),
            //     ),
            //   ),
            // if (!validConnection)
            //   Container(
            //     color: Colors.black.withOpacity(0.5),
            //     child: Column(
            //       mainAxisAlignment: MainAxisAlignment.center,
            //       children: [
            //         CircularProgressIndicator(),
            //         SizedBox(
            //             height:
            //                 20), // Spacing between CircularProgressIndicator and the list
            //         Text(
            //           'Failed Connections:',
            //           style: TextStyle(color: Colors.white),
            //         ),
            //         SizedBox(
            //             height:
            //                 10), // Spacing between 'Failed Connections' text and the list
            //         Expanded(
            //           child: ListView.builder(
            //             itemCount: failedUsers.length,
            //             itemBuilder: (context, index) {
            //               return ListTile(
            //                 title: Text(
            //                   failedUsers[index].name,
            //                   style: TextStyle(color: Colors.white),
            //                 ),
            //               );
            //             },
            //           ),
            //         ),
            //       ],
            //     ),
            //   )
            Positioned(
              bottom: 0,
              child: Container(
                height: 50,
                width: width,
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    IconButton(
                      onPressed: _toggleCamera,
                      icon: !isVideoOn
                          ? const Icon(Icons.videocam_off)
                          : const Icon(Icons.videocam),
                    ),
                    IconButton(
                      onPressed: _toggleMic,
                      icon: !isAudioOn
                          ? const Icon(Icons.mic_off)
                          : const Icon(Icons.mic),
                    ),
                    IconButton(
                      onPressed: _switchCamera,
                      icon: const Icon(Icons.switch_camera),
                    ),
                    IconButton(
                      onPressed: _endCall,
                      icon: const Icon(Icons.call_end),
                    ),
                    IconButton(
                        onPressed: () {
                          _showDeviceSelectionDialog(context);
                        },
                        icon: const Icon(Icons.devices))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildRemoteUsersBottomSheet() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 100.0),
      child: SlideTransition(
        position: Tween<Offset>(begin: const Offset(0, 1), end: Offset.zero)
            .animate(bottomSheetController),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Wrap(
            children: [
              Card(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                elevation: 4,
                margin: EdgeInsets.all(0),
                child: Padding(
                  padding: EdgeInsets.all(0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                          onTap: () {
                            bottomSheetController.reverse();
                          },
                          child: const Icon(Icons.cancel)),
                      AnimatedContainer(
                        duration: Duration(milliseconds: 300),
                        height: _isListVisible ? 200 : 0,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: users.length,
                          itemBuilder: (context, index) {
                            User user = users[index];
                            return Container(
                              child: user.renderer != null
                                  ? RemoteVideoCard(user, () {
                                      setState(() {
                                        user.fullScreenMode =
                                            !user.fullScreenMode;
                                      });
                                    }, localUserIsAdmin)
                                  : const SizedBox(),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectDevice(String deviceId, String kind) async {
    print("device id :::${deviceId}");
    print("type device :  ${kind}");
    print("the audio list  ${_audioDevices.toString()}");
    try {
      if (kind == 'audio') {
        _localStream!.getAudioTracks().forEach((track) async {
          // await _localStream!.removeTrack(track);
          await track.stop();
        });
        var newStream = await navigator.mediaDevices.getUserMedia({
          'audio': {
            if (deviceId != null && kIsWeb) 'deviceId': deviceId,
            if (deviceId != null && !kIsWeb)
              'optional': [
                {'sourceId': deviceId}
              ],
          },
          'video': true,
        });
        newStream.getAudioTracks().forEach((track) async {
          await _localStream!.addTrack(track);
        });
        _localVideoRenderer.srcObject = _localStream;
      } else if (kind == 'video') {
        print("videooo");
        // Stop and remove existing video tracks from _localStream
        _localStream!.getVideoTracks().forEach((track) async {
          await track.stop();
          // await _localStream!.removeTrack(track);
        });

        // Get new video stream by device ID
        MediaStream newStream = await navigator.mediaDevices.getUserMedia({
          'video': {
            if (deviceId != null && kIsWeb) 'deviceId': deviceId,
            if (deviceId != null && !kIsWeb)
              'optional': [
                {'sourceId': deviceId}
              ],
          },
          'audio': true,
        });
        newStream.getVideoTracks().forEach((track) async {
          await _localStream!.addTrack(track);
        });

        _localVideoRenderer.srcObject = _localStream;

        // // Replace video track for all peer connections
        // peers?.forEach((peerObj) {
        //   RTCPeerConnection peer = peerObj.peer;
        //   var connectionState = peer.connectionState;

        //   if (checkConnectionState(connectionState)) {
        //     RTCRtpSender? senderV = peer.getSenders().firstWhereOrNull((s) => s.track?.kind == "video");
        //     if (senderV != null) {
        //       senderV.replaceTrack(newStream.getVideoTracks()[0]);
        //     }
        //   }
        // });
      }

      setState(() {});
    } catch (e) {
      print('Error selecting device: $e');
    }
  }

  _showDeviceSelectionDialog(BuildContext context1) async {
    await _getMediaDevices();
    List<MediaDeviceInfo> audioDevices = _audioDevices;
    List<MediaDeviceInfo> videoDevices = _videoDevices;

    showDialog(
      context: context1,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Select Device'),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text('Audio Devices:'),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: audioDevices.map((device) {
                  return ListTile(
                    title: Text(device.label),
                    onTap: () {
                      _selectDevice(device.deviceId.toString(), 'audio');
                      Navigator.of(context1).pop(); // Close the dialog
                    },
                  );
                }).toList(),
              ),
              SizedBox(height: 10),
              Text('Video Devices:'),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: videoDevices.map((device) {
                  return ListTile(
                    title: Text(device.label),
                    onTap: () {
                      _selectDevice(device.deviceId.toString(), 'video');
                      Navigator.of(context1).pop(); // Close the dialog
                    },
                  );
                }).toList(),
              ),
            ],
          ),
        );
      },
    );
  }
}
