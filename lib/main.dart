import 'package:flutter/material.dart';
import 'package:flutter_mesh2/core/pages/home.dart';
import 'package:flutter_mesh2/core/utils/signaling.dart';

void main() {
  // String baseUrl = 'http://192.168.1.12:3001';
  String baseUrl = 'https://yorkbritishacademy.net/';
  Signaling.instance.init(baseUrl);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Mesh Video Call',
      home: Home(),
      debugShowCheckedModeBanner: false,
    );
  }
}
